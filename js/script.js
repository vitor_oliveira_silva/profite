BANNER = (function($){
            
    var p = {};
    p.t = null;
    p.obj = null;
    p.cont = 1;
    p.limit = 1;
    p.tempSlide = 5000;
    p.tempEfeito = 1000;

    
    p.init = function(objParam, tempSlide, tempEfeito){

        if(typeof tempEfeito !== 'undefined' && tempEfeito !== null){
           p.tempEfeito = tempEfeito;
        }
        if(typeof tempSlide !== 'undefined' && tempSlide !== null){
           p.tempSlide = tempSlide;
        }

        p.obj = objParam
        p.limit = $(p.obj).children('.banner').children().length;

        var html = '<ul class="banner-controller">';
        for(x=0;x<p.limit;x++){
            classe = '';
            if(x==0){
                classe = ' active ';
            }
            html += '<li><a href="javascript:;" onclick="BANNER.pulo('+x+');" class="bt-controller-'+x+' '+classe+'"></a></li>';
        }
        html += '</ul>';
        $(p.obj).prepend(html);

        var w = (100/p.limit);
        $(p.obj+' .banner').css({'width':(100*p.limit)+'%'});
        $(p.obj+' .banner li').css({'float' : 'left','width' : w+'%'});
        $(p.obj).css({'overflow' : 'hidden','position':'relative'});

        p.play();
    }

    p.reseta = function(){
        p.cont = 0;
        p.btActive();
        p.cont = 1;
        $(p.obj).children('.banner').animate({marginLeft:'0px'}, p.tempEfeito);
    }

    p.play = function(){
        p.t = setInterval(function(){
            if(p.cont<p.limit){
                p.anima();
            }else{
                p.reseta();
                console.log('final...');
            }
        },p.tempSlide);
    }
    p.pulo = function(cont){
        p.pause();
        p.cont = cont;
        p.anima();
        p.play();
    }

    p.pause = function(){
        clearInterval(p.t);
    }

    p.anima = function(){
        p.btActive();
        $(p.obj).children('.banner').animate({marginLeft:-p.cont+'00%'}, p.tempEfeito);
        p.cont++;
    }
    p.btActive = function(){
        $('a[class^=bt-controller-]').removeClass('active');
        $('.bt-controller-'+p.cont).addClass('active');                
    }

    return p;

}(jQuery));

$(function(){
    
    BANNER.init('.banner-slide', 5000,1000);

    $('.rank a.star').click(function(e){
        e.preventDefault();
    });
});